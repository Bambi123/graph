#include "gdwg/graph.hpp"

#include <catch2/catch.hpp>

/*
Thomas French
2nd of August
COMP6771 Assignment 3
graph_test_constructors.cpp

The following testing file, will detail the possible testing scenarios for the
constructor methods for the class file graph.hpp.  The tests are designed based on
individual function calls and sections are designed based on specific scenarios
that could occur whilst using the library.  Edge cases are included in these
testing scenarios.
*/

TEST_CASE("Default Constructor") {
	SECTION("Zero Nodes: <int, int>") {
		auto g = gdwg::graph<int, int>{};
		CHECK(!g.is_node(1));
	}

	SECTION("Empty: <int, int>") {
		auto g = gdwg::graph<double, double>{};
		CHECK(g.empty());
	}

	SECTION("Zero Nodes: <double, double>") {
		auto g = gdwg::graph<double, double>{};
		CHECK(!g.is_node(1.1));
	}

	SECTION("Empty: <double, double>") {
		auto g = gdwg::graph<double, double>{};
		CHECK(g.empty());
	}

	SECTION("Zero Nodes: <std::string, std::string>") {
		auto g = gdwg::graph<std::string, std::string>{};
		CHECK(!g.is_node("hello"));
	}

	SECTION("Empty: <std::string, std::string>") {
		auto g = gdwg::graph<std::string, std::string>{};
		CHECK(g.empty());
	}

	SECTION("Zero Nodes: <?, ?>") {
		auto g = gdwg::graph<double, std::string>{};
		CHECK(!g.is_node(1.5));
	}

	SECTION("Empty: <?, ?>") {
		auto g = gdwg::graph<double, std::string>{};
		CHECK(g.empty());
	}
}

TEST_CASE("Initializer List Constructor") {
	SECTION("Nodes Initializer: <int, int>") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));
	}

	SECTION("Not Empty: <int, int>") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(!g.empty());
	}

	SECTION("Nodes Initializer: <double, double>") {
		auto g = gdwg::graph<double, double>{1.1, 2.2, 3.3};

		CHECK(g.is_node(1.1));
		CHECK(g.is_node(2.2));
		CHECK(g.is_node(3.3));
	}

	SECTION("Not Empty: <double, double>") {
		auto g = gdwg::graph<double, double>{1.1, 2.2, 3.3};

		CHECK(!g.empty());
	}

	SECTION("Nodes Initializer: <std::string, std::string>") {
		auto g = gdwg::graph<std::string, std::string>{"hi", "nice", "bye"};

		CHECK(g.is_node("hi"));
		CHECK(g.is_node("nice"));
		CHECK(g.is_node("bye"));
	}

	SECTION("Not Empty: <std::string, std::string>") {
		auto g = gdwg::graph<std::string, std::string>{"hi", "nice", "bye"};

		CHECK(!g.empty());
	}

	SECTION("Nodes Initializer: <?, ?>") {
		auto g = gdwg::graph<double, std::string>{1.1, 2.2, 3.3};

		CHECK(g.is_node(1.1));
		CHECK(g.is_node(2.2));
		CHECK(g.is_node(3.3));
	}

	SECTION("Not Empty: <?, ?>") {
		auto g = gdwg::graph<double, std::string>{1.1, 2.2, 3.3};

		CHECK(!g.empty());
	}
}

TEST_CASE("Input Iterator Constructor") {
	SECTION("Nodes Initializer: <int, int>") {
		auto v = std::vector<int>{1, 2, 3};
		auto g = gdwg::graph<int, int>(v.begin(), v.end());

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));
	}

	SECTION("Not Empty: <int, int>") {
		auto v = std::vector<int>{1, 2, 3};
		auto g = gdwg::graph<int, int>(v.begin(), v.end());

		CHECK(!g.empty());
	}

	SECTION("Nodes Initializer: <double, double>") {
		auto v = std::vector<double>{1.1, 2.2, 3.3};
		auto g = gdwg::graph<double, double>(v.begin(), v.end());

		CHECK(g.is_node(1.1));
		CHECK(g.is_node(2.2));
		CHECK(g.is_node(3.3));
	}

	SECTION("Not Empty: <double, double>") {
		auto v = std::vector<double>{1.1, 2.2, 3.3};
		auto g = gdwg::graph<double, double>(v.begin(), v.end());

		CHECK(!g.empty());
	}

	SECTION("Nodes Initializer: <std::string, std::string>") {
		auto v = std::vector<std::string>{"hi", "nice", "bye"};
		auto g = gdwg::graph<std::string, std::string>(v.begin(), v.end());

		CHECK(g.is_node("hi"));
		CHECK(g.is_node("nice"));
		CHECK(g.is_node("bye"));
	}

	SECTION("Not Empty: <std::string, std::string>") {
		auto g = gdwg::graph<std::string, std::string>{"hi", "nice", "bye"};

		CHECK(!g.empty());
	}

	SECTION("Nodes Initializer: <?, ?>") {
		auto v = std::vector<double>{1.1, 2.2, 3.3};
		auto g = gdwg::graph<double, std::string>(v.begin(), v.end());

		CHECK(g.is_node(1.1));
		CHECK(g.is_node(2.2));
		CHECK(g.is_node(3.3));
	}

	SECTION("Not Empty: <?, ?>") {
		auto v = std::vector<double>{1.1, 2.2, 3.3};
		auto g = gdwg::graph<double, std::string>(v.begin(), v.end());

		CHECK(!g.empty());
	}
}

TEST_CASE("Graph Move Constructor") {
	SECTION("Graph Moved: <int, int>") {
		auto g1 = gdwg::graph<int, int>{1, 2, 3};
		CHECK(g1.is_node(1));
		CHECK(g1.is_node(2));
		CHECK(g1.is_node(3));
		CHECK(!g1.is_node(4));

		CHECK(g1.insert_edge(1, 2, 10));
		CHECK(g1.insert_edge(2, 3, 15));
		CHECK(g1.insert_edge(3, 1, 20));

		auto g = gdwg::graph(std::move(g1));

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));
		CHECK(!g.is_node(4));

		CHECK(g.edge_exists(1, 2, 10));
		CHECK(g.edge_exists(2, 3, 15));
		CHECK(g.edge_exists(3, 1, 20));

		CHECK(g1.empty());
	}

	SECTION("Graph Moved: <double, double>") {
		auto g1 = gdwg::graph<double, double>{1.1, 2.2, 3.3};
		CHECK(g1.is_node(1.1));
		CHECK(g1.is_node(2.2));
		CHECK(g1.is_node(3.3));
		CHECK(!g1.is_node(4.4));

		CHECK(g1.insert_edge(1.1, 2.2, 10.5));
		CHECK(g1.insert_edge(2.2, 3.3, 15.5));
		CHECK(g1.insert_edge(3.3, 1.1, 20.5));

		auto g = gdwg::graph(std::move(g1));

		CHECK(g.is_node(1.1));
		CHECK(g.is_node(2.2));
		CHECK(g.is_node(3.3));
		CHECK(!g.is_node(4.4));

		CHECK(g.edge_exists(1.1, 2.2, 10.5));
		CHECK(g.edge_exists(2.2, 3.3, 15.5));
		CHECK(g.edge_exists(3.3, 1.1, 20.5));

		CHECK(g1.empty());
	}

	SECTION("Graph Moved: <std::string, std::string>") {
		auto g1 = gdwg::graph<std::string, std::string>{"hi", "nice", "bye"};
		CHECK(g1.is_node("hi"));
		CHECK(g1.is_node("nice"));
		CHECK(g1.is_node("bye"));
		CHECK(!g1.is_node("lol"));

		CHECK(g1.insert_edge("hi", "nice", "weight1"));
		CHECK(g1.insert_edge("nice", "bye", "weight2"));
		CHECK(g1.insert_edge("bye", "hi", "weight3"));

		auto g = gdwg::graph(std::move(g1));

		CHECK(g.is_node("hi"));
		CHECK(g.is_node("nice"));
		CHECK(g.is_node("bye"));
		CHECK(!g.is_node("lol"));

		CHECK(g.edge_exists("hi", "nice", "weight1"));
		CHECK(g.edge_exists("nice", "bye", "weight2"));
		CHECK(g.edge_exists("bye", "hi", "weight3"));

		CHECK(g1.empty());
	}

	SECTION("Graph Moved: <?, ?>") {
		auto g1 = gdwg::graph<double, std::string>{1.1, 2.2, 3.3};
		CHECK(g1.is_node(1.1));
		CHECK(g1.is_node(2.2));
		CHECK(g1.is_node(3.3));
		CHECK(!g1.is_node(4.4));

		CHECK(g1.insert_edge(1.1, 2.2, "weight1"));
		CHECK(g1.insert_edge(2.2, 3.3, "weight2"));
		CHECK(g1.insert_edge(3.3, 1.1, "weight3"));

		auto g = gdwg::graph(std::move(g1));

		CHECK(g.is_node(1.1));
		CHECK(g.is_node(2.2));
		CHECK(g.is_node(3.3));
		CHECK(!g.is_node(4.4));

		CHECK(g.edge_exists(1.1, 2.2, "weight1"));
		CHECK(g.edge_exists(2.2, 3.3, "weight2"));
		CHECK(g.edge_exists(3.3, 1.1, "weight3"));

		CHECK(g1.empty());
	}
}

TEST_CASE("Equals Move Constructor") {
	SECTION("Equals Graph Moved: <int, int>") {
		auto g1 = gdwg::graph<int, int>{1, 2, 3};
		CHECK(g1.is_node(1));
		CHECK(g1.is_node(2));
		CHECK(g1.is_node(3));
		CHECK(!g1.is_node(4));

		CHECK(g1.insert_edge(1, 2, 10));
		CHECK(g1.insert_edge(2, 3, 15));
		CHECK(g1.insert_edge(3, 1, 20));

		auto g = std::move(g1);

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));
		CHECK(!g.is_node(4));

		CHECK(g.edge_exists(1, 2, 10));
		CHECK(g.edge_exists(2, 3, 15));
		CHECK(g.edge_exists(3, 1, 20));

		CHECK(g1.empty());
	}

	SECTION("Equals Graph Moved: <double, double>") {
		auto g1 = gdwg::graph<double, double>{1.1, 2.2, 3.3};
		CHECK(g1.is_node(1.1));
		CHECK(g1.is_node(2.2));
		CHECK(g1.is_node(3.3));
		CHECK(!g1.is_node(4.4));

		CHECK(g1.insert_edge(1.1, 2.2, 10.5));
		CHECK(g1.insert_edge(2.2, 3.3, 15.5));
		CHECK(g1.insert_edge(3.3, 1.1, 20.5));

		auto g = std::move(g1);

		CHECK(g.is_node(1.1));
		CHECK(g.is_node(2.2));
		CHECK(g.is_node(3.3));
		CHECK(!g.is_node(4.4));

		CHECK(g.edge_exists(1.1, 2.2, 10.5));
		CHECK(g.edge_exists(2.2, 3.3, 15.5));
		CHECK(g.edge_exists(3.3, 1.1, 20.5));

		CHECK(g1.empty());
	}

	SECTION("Equals Graph Moved: <std::string, std::string>") {
		auto g1 = gdwg::graph<std::string, std::string>{"hi", "nice", "bye"};
		CHECK(g1.is_node("hi"));
		CHECK(g1.is_node("nice"));
		CHECK(g1.is_node("bye"));
		CHECK(!g1.is_node("lol"));

		CHECK(g1.insert_edge("hi", "nice", "weight1"));
		CHECK(g1.insert_edge("nice", "bye", "weight2"));
		CHECK(g1.insert_edge("bye", "hi", "weight3"));

		auto g = std::move(g1);

		CHECK(g.is_node("hi"));
		CHECK(g.is_node("nice"));
		CHECK(g.is_node("bye"));
		CHECK(!g.is_node("lol"));

		CHECK(g.edge_exists("hi", "nice", "weight1"));
		CHECK(g.edge_exists("nice", "bye", "weight2"));
		CHECK(g.edge_exists("bye", "hi", "weight3"));

		CHECK(g1.empty());
	}

	SECTION("Equals Graph Moved: <?, ?>") {
		auto g1 = gdwg::graph<double, std::string>{1.1, 2.2, 3.3};
		CHECK(g1.is_node(1.1));
		CHECK(g1.is_node(2.2));
		CHECK(g1.is_node(3.3));
		CHECK(!g1.is_node(4.4));

		CHECK(g1.insert_edge(1.1, 2.2, "weight1"));
		CHECK(g1.insert_edge(2.2, 3.3, "weight2"));
		CHECK(g1.insert_edge(3.3, 1.1, "weight3"));

		auto g = std::move(g1);

		CHECK(g.is_node(1.1));
		CHECK(g.is_node(2.2));
		CHECK(g.is_node(3.3));
		CHECK(!g.is_node(4.4));

		CHECK(g.edge_exists(1.1, 2.2, "weight1"));
		CHECK(g.edge_exists(2.2, 3.3, "weight2"));
		CHECK(g.edge_exists(3.3, 1.1, "weight3"));

		CHECK(g1.empty());
	}
}

TEST_CASE("Graph Copy Constructor") {
	SECTION("Graph Copy: <int, int>") {
		auto g1 = gdwg::graph<int, int>{1, 2, 3};
		CHECK(g1.is_node(1));
		CHECK(g1.is_node(2));
		CHECK(g1.is_node(3));
		CHECK(!g1.is_node(4));

		CHECK(g1.insert_edge(1, 2, 10));
		CHECK(g1.insert_edge(2, 3, 15));
		CHECK(g1.insert_edge(3, 1, 20));

		auto g = gdwg::graph(g1);

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));
		CHECK(!g.is_node(4));

		CHECK(g.edge_exists(1, 2, 10));
		CHECK(g.edge_exists(2, 3, 15));
		CHECK(g.edge_exists(3, 1, 20));

		CHECK(!g1.empty());

		CHECK(g == g1);
	}

	SECTION("Graph Copy: <double, double>") {
		auto g1 = gdwg::graph<double, double>{1.1, 2.2, 3.3};
		CHECK(g1.is_node(1.1));
		CHECK(g1.is_node(2.2));
		CHECK(g1.is_node(3.3));
		CHECK(!g1.is_node(4.4));

		CHECK(g1.insert_edge(1.1, 2.2, 10.5));
		CHECK(g1.insert_edge(2.2, 3.3, 15.5));
		CHECK(g1.insert_edge(3.3, 1.1, 20.5));

		auto g = gdwg::graph(g1);

		CHECK(g.is_node(1.1));
		CHECK(g.is_node(2.2));
		CHECK(g.is_node(3.3));
		CHECK(!g.is_node(4.4));

		CHECK(g.edge_exists(1.1, 2.2, 10.5));
		CHECK(g.edge_exists(2.2, 3.3, 15.5));
		CHECK(g.edge_exists(3.3, 1.1, 20.5));

		CHECK(!g1.empty());

		CHECK(g == g1);
	}

	SECTION("Graph Copy: <std::string, std::string>") {
		auto g1 = gdwg::graph<std::string, std::string>{"hi", "nice", "bye"};
		CHECK(g1.is_node("hi"));
		CHECK(g1.is_node("nice"));
		CHECK(g1.is_node("bye"));
		CHECK(!g1.is_node("lol"));

		CHECK(g1.insert_edge("hi", "nice", "weight1"));
		CHECK(g1.insert_edge("nice", "bye", "weight2"));
		CHECK(g1.insert_edge("bye", "hi", "weight3"));

		auto g = gdwg::graph(g1);

		CHECK(g.is_node("hi"));
		CHECK(g.is_node("nice"));
		CHECK(g.is_node("bye"));
		CHECK(!g.is_node("lol"));

		CHECK(g.edge_exists("hi", "nice", "weight1"));
		CHECK(g.edge_exists("nice", "bye", "weight2"));
		CHECK(g.edge_exists("bye", "hi", "weight3"));

		CHECK(!g1.empty());

		CHECK(g == g1);
	}

	SECTION("Graph Copy: <?, ?>") {
		auto g1 = gdwg::graph<double, std::string>{1.1, 2.2, 3.3};
		CHECK(g1.is_node(1.1));
		CHECK(g1.is_node(2.2));
		CHECK(g1.is_node(3.3));
		CHECK(!g1.is_node(4.4));

		CHECK(g1.insert_edge(1.1, 2.2, "weight1"));
		CHECK(g1.insert_edge(2.2, 3.3, "weight2"));
		CHECK(g1.insert_edge(3.3, 1.1, "weight3"));

		auto g = gdwg::graph(g1);

		CHECK(g.is_node(1.1));
		CHECK(g.is_node(2.2));
		CHECK(g.is_node(3.3));
		CHECK(!g.is_node(4.4));

		CHECK(g.edge_exists(1.1, 2.2, "weight1"));
		CHECK(g.edge_exists(2.2, 3.3, "weight2"));
		CHECK(g.edge_exists(3.3, 1.1, "weight3"));

		CHECK(!g1.empty());

		CHECK(g == g1);
	}
}

TEST_CASE("Equals Copy Constructor") {
	SECTION("Equals Graph Copy: <int, int>") {
		auto g1 = gdwg::graph<int, int>{1, 2, 3};
		CHECK(g1.is_node(1));
		CHECK(g1.is_node(2));
		CHECK(g1.is_node(3));
		CHECK(!g1.is_node(4));

		CHECK(g1.insert_edge(1, 2, 10));
		CHECK(g1.insert_edge(2, 3, 15));
		CHECK(g1.insert_edge(3, 1, 20));

		auto g = g1;

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));
		CHECK(!g.is_node(4));

		CHECK(g.edge_exists(1, 2, 10));
		CHECK(g.edge_exists(2, 3, 15));
		CHECK(g.edge_exists(3, 1, 20));

		CHECK(!g1.empty());

		CHECK(g == g1);
	}

	SECTION("Equals Graph Copy: <double, double>") {
		auto g1 = gdwg::graph<double, double>{1.1, 2.2, 3.3};
		CHECK(g1.is_node(1.1));
		CHECK(g1.is_node(2.2));
		CHECK(g1.is_node(3.3));
		CHECK(!g1.is_node(4.4));

		CHECK(g1.insert_edge(1.1, 2.2, 10.5));
		CHECK(g1.insert_edge(2.2, 3.3, 15.5));
		CHECK(g1.insert_edge(3.3, 1.1, 20.5));

		auto g = g1;

		CHECK(g.is_node(1.1));
		CHECK(g.is_node(2.2));
		CHECK(g.is_node(3.3));
		CHECK(!g.is_node(4.4));

		CHECK(g.edge_exists(1.1, 2.2, 10.5));
		CHECK(g.edge_exists(2.2, 3.3, 15.5));
		CHECK(g.edge_exists(3.3, 1.1, 20.5));

		CHECK(!g1.empty());

		CHECK(g == g1);
	}

	SECTION("Equals Graph Copy: <std::string, std::string>") {
		auto g1 = gdwg::graph<std::string, std::string>{"hi", "nice", "bye"};
		CHECK(g1.is_node("hi"));
		CHECK(g1.is_node("nice"));
		CHECK(g1.is_node("bye"));
		CHECK(!g1.is_node("lol"));

		CHECK(g1.insert_edge("hi", "nice", "weight1"));
		CHECK(g1.insert_edge("nice", "bye", "weight2"));
		CHECK(g1.insert_edge("bye", "hi", "weight3"));

		auto g = g1;

		CHECK(g.is_node("hi"));
		CHECK(g.is_node("nice"));
		CHECK(g.is_node("bye"));
		CHECK(!g.is_node("lol"));

		CHECK(g.edge_exists("hi", "nice", "weight1"));
		CHECK(g.edge_exists("nice", "bye", "weight2"));
		CHECK(g.edge_exists("bye", "hi", "weight3"));

		CHECK(!g1.empty());

		CHECK(g == g1);
	}

	SECTION("Equals Graph Copy: <?, ?>") {
		auto g1 = gdwg::graph<double, std::string>{1.1, 2.2, 3.3};
		CHECK(g1.is_node(1.1));
		CHECK(g1.is_node(2.2));
		CHECK(g1.is_node(3.3));
		CHECK(!g1.is_node(4.4));

		CHECK(g1.insert_edge(1.1, 2.2, "weight1"));
		CHECK(g1.insert_edge(2.2, 3.3, "weight2"));
		CHECK(g1.insert_edge(3.3, 1.1, "weight3"));

		auto g = g1;

		CHECK(g.is_node(1.1));
		CHECK(g.is_node(2.2));
		CHECK(g.is_node(3.3));
		CHECK(!g.is_node(4.4));

		CHECK(g.edge_exists(1.1, 2.2, "weight1"));
		CHECK(g.edge_exists(2.2, 3.3, "weight2"));
		CHECK(g.edge_exists(3.3, 1.1, "weight3"));

		CHECK(!g1.empty());

		CHECK(g == g1);
	}
}
