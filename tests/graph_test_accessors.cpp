#include "gdwg/graph.hpp"

#include <catch2/catch.hpp>

/*
Thomas French
2nd of August
COMP6771 Assignment 3
graph_test_accessors.cpp

The following testing file, will detail the possible testing scenarios for the
accessor methods for the class file graph.hpp.  The tests are designed based on
individual function calls and sections are designed based on specific scenarios
that could occur whilst using the library.  Edge cases are included in these
testing scenarios.
*/

TEST_CASE("Node Exists") {
	SECTION("The Nodes Exist") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));
	}
}

TEST_CASE("Empty") {
	SECTION("The Graph is Empty") {
		auto g = gdwg::graph<int, int>{};

		CHECK(g.empty());
		CHECK(g.insert_node(1));
		CHECK(!g.empty());
	}
}

TEST_CASE("Connected Nodes") {
	SECTION("Connected is used Correctly") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(2, 3, 15));

		CHECK(g.is_connected(1, 2));
		CHECK(g.is_connected(2, 3));
	}

	SECTION("Connected but SRC doesn't exist") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(2, 3, 15));

		CHECK_THROWS(g.is_connected(4, 1));
	}

	SECTION("Connected but DST doesn't exist") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(2, 3, 15));

		CHECK_THROWS(g.is_connected(1, 4));
	}

	SECTION("Connected but DST and SRC doesn't exist") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(2, 3, 15));

		CHECK_THROWS(g.is_connected(5, 4));
	}

	SECTION("Connected but no connection is found") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(2, 3, 15));

		CHECK(!g.is_connected(1, 3));
	}
}

TEST_CASE("Vector of Nodes") {
	SECTION("Vector Nodes is used correctly") {
		auto g = gdwg::graph<int, int>{1, 3, 2};
		auto v = g.nodes();
		auto it = v.begin();

		CHECK(*it == 1);
		CHECK(*++it == 2);
		CHECK(*++it == 3);
	}

	SECTION("Vector Nodes but for an Empty Graph") {
		auto g = gdwg::graph<int, int>{};
		auto v = g.nodes();
		auto it = v.begin();

		CHECK(it == v.end());
	}
}

TEST_CASE("Vector of Edges") {
	SECTION("Vector Edges is used correctly") {
		auto g = gdwg::graph<int, int>{1, 3, 2};

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 2, 12));
		CHECK(g.insert_edge(1, 2, 11));
		CHECK(g.insert_edge(2, 3, 15));

		auto v = g.weights(1, 2);
		auto it = v.begin();
		CHECK(*it == 10);
		CHECK(*++it == 11);
		CHECK(*++it == 12);
	}

	SECTION("Vector Edges but for an Empty graph") {
		auto g = gdwg::graph<int, int>{};

		CHECK_THROWS(g.weights(1, 2));
	}

	SECTION("Vector Edges but SRC and DST node doesn't Exist") {
		auto g = gdwg::graph<int, int>{1, 3, 2};

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 2, 12));
		CHECK(g.insert_edge(1, 2, 11));
		CHECK(g.insert_edge(2, 3, 15));

		CHECK_THROWS(g.weights(4, 5));
	}

	SECTION("Vector Edges but SRC node doesn't Exist") {
		auto g = gdwg::graph<int, int>{1, 3, 2};

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 2, 12));
		CHECK(g.insert_edge(1, 2, 11));
		CHECK(g.insert_edge(2, 3, 15));

		CHECK_THROWS(g.weights(4, 1));
	}

	SECTION("Vector Edges but DST node doesn't Exist") {
		auto g = gdwg::graph<int, int>{1, 3, 2};

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 2, 12));
		CHECK(g.insert_edge(1, 2, 11));
		CHECK(g.insert_edge(2, 3, 15));

		CHECK_THROWS(g.weights(1, 4));
	}
}

TEST_CASE("Vector of Connections") {
	SECTION("Vector Connections used correctly") {
		auto g = gdwg::graph<int, int>{1, 3, 2};

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 2, 12));
		CHECK(g.insert_edge(1, 2, 11));
		CHECK(g.insert_edge(1, 3, 12));

		auto v = g.connections(1);
		auto it = v.begin();
		CHECK(*it == 2);
		CHECK(*++it == 3);
		CHECK(++it == v.end());
	}

	SECTION("Vector Connections but SRC node doesn't exist") {
		auto g = gdwg::graph<int, int>{1, 3, 2};

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 2, 12));
		CHECK(g.insert_edge(1, 2, 11));
		CHECK(g.insert_edge(1, 3, 12));

		CHECK_THROWS(g.connections(4));
	}
}
