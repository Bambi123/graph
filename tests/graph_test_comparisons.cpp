#include "gdwg/graph.hpp"

#include <catch2/catch.hpp>

/*
Thomas French
2nd of August
COMP6771 Assignment 3
graph_test_comparisons.cpp

The following testing file, will detail the possible testing scenarios for the
comparison methods for the class file graph.hpp.  The tests are designed based on
individual function calls and sections are designed based on specific scenarios
that could occur whilst using the library.  Edge cases are included in these
testing scenarios.
*/

TEST_CASE("Graphs ARE Equal") {
	auto g = gdwg::graph<int, int>{1, 2, 3};
	CHECK(g.insert_edge(1, 2, 10));
	CHECK(g.insert_edge(1, 3, 15));
	CHECK(g.insert_edge(2, 3, 20));

	auto w = gdwg::graph<int, int>{1, 2, 3};
	CHECK(w.insert_edge(1, 2, 10));
	CHECK(w.insert_edge(1, 3, 15));
	CHECK(w.insert_edge(2, 3, 20));

	CHECK(g == w);
}

TEST_CASE("Graphs ARE NOT Equal") {
	SECTION("Not Equal") {
		auto g = gdwg::graph<int, int>{1, 2, 3};
		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 3, 15));
		CHECK(g.insert_edge(2, 3, 20));

		auto w = gdwg::graph<int, int>{4, 5, 6};
		CHECK(w.insert_edge(4, 5, 10));
		CHECK(w.insert_edge(4, 6, 15));
		CHECK(w.insert_edge(5, 6, 20));

		CHECK(!(g == w));
	}

	SECTION("Not Equal, after Replace Node") {
		auto g = gdwg::graph<int, int>{1, 2, 3};
		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 3, 15));
		CHECK(g.insert_edge(2, 3, 20));

		auto w = g;
		CHECK(w.replace_node(2, 4));

		CHECK(w.edge_exists(1, 4, 10));
		CHECK(w.edge_exists(1, 3, 15));
		CHECK(w.edge_exists(4, 3, 20));

		CHECK(!(g == w));
	}
}
