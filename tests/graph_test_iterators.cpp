#include "gdwg/graph.hpp"

#include <catch2/catch.hpp>

/*
Thomas French
2nd of August
COMP6771 Assignment 3
graph_test_iterator.cpp

The following testing file, will detail the possible testing scenarios for the
iterator methods for the class file graph.hpp.  The tests are designed based on
individual function calls and sections are designed based on specific scenarios
that could occur whilst using the library.  Edge cases are included in these
testing scenarios.
*/

TEST_CASE("Iterator Constructor") {
	SECTION("Default Iterator Construction") {
		auto it = gdwg::graph<int, int>::iterator{};
	}

	SECTION("Iterator Condtructed Correctly") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, "weight1"));

		auto it = g.begin();
		auto v = *it;
		CHECK(v.from == 1);
		CHECK(v.to == 2);
		CHECK(v.weight == "weight1");
	}
}

TEST_CASE("Begin Iterator") {
	SECTION("Begin Iterator used Correctly") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, "weight1"));

		auto it = g.begin();
		auto v = *it;
		CHECK(v.from == 1);
		CHECK(v.to == 2);
		CHECK(v.weight == "weight1");
	}

	SECTION("Begin Iterator used on a Graph with no Edges") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		CHECK(!g.empty());

		CHECK(g.begin() == g.end());
	}
}

TEST_CASE("End Iterator") {
	SECTION("End Iterator used Correctly") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, "weight1"));
		CHECK(g.insert_edge(1, 3, "weight2"));
		CHECK(g.insert_edge(2, 3, "weight3"));

		auto it = g.end();
		CHECK(g.end() == it);
	}

	SECTION("End Iterator used on an Graph with no Edges") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		auto it = g.end();
		CHECK(g.end() == it);
	}
}

TEST_CASE("Dereference Iterator") {
	SECTION("Dereference Iterator used Correctly") {}

	SECTION("Dereference Iterator used on Begin Iterator") {}

	SECTION("Dereference Iterator used on End Iterator") {}

	SECTION("Derefence Iterator used on an Graph with no Edge") {}
}

TEST_CASE("Increment Iterator") {
	SECTION("Increment Iterator used on Begin Iterator") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, "weight1"));
		CHECK(g.insert_edge(1, 3, "weight2"));
		CHECK(g.insert_edge(2, 3, "weight3"));

		auto it = g.begin();
		auto v = *(++it);
		CHECK(v.from == 1);
		CHECK(v.to == 3);
		CHECK(v.weight == "weight2");

		v = *(it++);
		CHECK(v.from == 1);
		CHECK(v.to == 3);
		CHECK(v.weight == "weight2");

		v = *it;
		CHECK(v.from == 2);
		CHECK(v.to == 3);
		CHECK(v.weight == "weight3");
	}

	SECTION("Increment Iterator used on End Iterator") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, "weight1"));
		CHECK(g.insert_edge(1, 3, "weight2"));
		CHECK(g.insert_edge(2, 3, "weight3"));

		auto it = g.end();
		++it;
		// check that iterator is unchanged
		CHECK(it == g.end());

		it = g.end();
		it++;
		// check that iterator is unchanged
		CHECK(it == g.end());
	}

	SECTION("Increment Iterator used on Graph with no Edges") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		auto it = g.begin();
		++it;

		CHECK(it == g.begin());
		CHECK(it == g.end());

		it = g.begin();
		it++;

		CHECK(it == g.begin());
		CHECK(it == g.end());
	}
}

TEST_CASE("Decrement Iterator") {
	SECTION("Decrement used on End Iterator") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, "weight1"));
		CHECK(g.insert_edge(1, 3, "weight2"));
		CHECK(g.insert_edge(2, 3, "weight3"));

		auto it = g.end();
		auto v = *(--it);
		CHECK(v.from == 2);
		CHECK(v.to == 3);
		CHECK(v.weight == "weight3");

		v = *(it--);
		CHECK(v.from == 2);
		CHECK(v.to == 3);
		CHECK(v.weight == "weight3");

		v = *it;
		CHECK(v.from == 1);
		CHECK(v.to == 3);
		CHECK(v.weight == "weight2");
	}
}

TEST_CASE("Equality Iterator") {
	SECTION("Equality Iterator used Correctly") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, "weight1"));
		CHECK(g.insert_edge(1, 3, "weight2"));
		CHECK(g.insert_edge(2, 3, "weight3"));

		auto it = g.begin();
		auto v = ++it;

		auto et = g.end();
		--et;
		auto w = --et;

		CHECK(v == w);
	}

	SECTION("Equality Iterator used on a Graph with no Edges") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		auto it = g.begin();

		auto et = g.end();

		CHECK(it == et);
	}
}

TEST_CASE("Find Iterator") {
	SECTION("Find Iterator used Correctly") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, "weight1"));
		CHECK(g.insert_edge(1, 3, "weight2"));
		CHECK(g.insert_edge(2, 3, "weight3"));

		auto it = g.find(1, 3, "weight2");

		CHECK(!(it == g.end()));
		auto v = *it;
		CHECK(v.from == 1);
		CHECK(v.to == 3);
		CHECK(v.weight == "weight2");
	}

	SECTION("Find Iterator used on non-existent Edge") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, "weight1"));
		CHECK(g.insert_edge(1, 3, "weight2"));
		CHECK(g.insert_edge(2, 3, "weight3"));

		auto it = g.find(1, 2, "weight10");

		CHECK(it == g.end());
	}

	SECTION("Find Iterator used on a Graph with no Edges") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		auto it = g.find(1, 2, "weight10");

		CHECK(it == g.end());
	}
}

TEST_CASE("Erase Edge Iterator") {
	SECTION("Erase Edge Iterator used Correctly") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, "weight1"));
		CHECK(g.insert_edge(1, 3, "weight2"));
		CHECK(g.insert_edge(2, 3, "weight3"));

		auto it = g.begin();

		auto check = g.erase_edge(it);
		auto v = *check;
		CHECK(v.from == 1);
		CHECK(v.to == 3);
		CHECK(v.weight == "weight2");

		CHECK(!g.edge_exists(1, 2, "weight1"));
	}

	SECTION("Erase Edge Iterator called on End Iterator") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, "weight1"));
		CHECK(g.insert_edge(1, 3, "weight2"));
		CHECK(g.insert_edge(2, 3, "weight3"));

		auto it = g.erase_edge(g.end());
		CHECK(it == g.end());
	}

	SECTION("Erase Edge Iterator but next element is returned") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, "weight1"));
		CHECK(g.insert_edge(1, 3, "weight2"));
		CHECK(g.insert_edge(2, 3, "weight3"));

		auto it = g.erase_edge(g.begin());
		auto v = *it;

		CHECK(v.from == 1);
		CHECK(v.to == 3);
		CHECK(v.weight == "weight2");
	}
}

TEST_CASE("Range Erase Edge Iterator") {
	SECTION("Range Erase Edge Iterator used Correctly") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3, 4};

		CHECK(g.insert_edge(1, 2, "weight1"));
		CHECK(g.insert_edge(1, 3, "weight2")); // START
		CHECK(g.insert_edge(1, 4, "weight3"));
		CHECK(g.insert_edge(2, 3, "weight4")); // END
		CHECK(g.insert_edge(2, 4, "weight5"));
		CHECK(g.insert_edge(3, 4, "weight6"));

		auto start = g.begin();
		start++;

		auto end = g.end();
		end--;
		end--;
		end--;

		auto v = *start;
		auto w = *end;

		CHECK(v.from == 1);
		CHECK(v.to == 3);
		CHECK(v.weight == "weight2");

		CHECK(w.from == 2);
		CHECK(w.to == 3);
		CHECK(w.weight == "weight4");

		auto it = g.erase_edge(start, end);
		v = *it;
		CHECK(v.from == 2);
		CHECK(v.to == 3);
		CHECK(v.weight == "weight4");
	}

	SECTION("Range Erase Edge Iterator used when neither Iterators Exist") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3, 4};

		CHECK(g.insert_edge(1, 2, "weight1"));
		CHECK(g.insert_edge(1, 3, "weight2"));
		CHECK(g.insert_edge(1, 4, "weight3"));
		CHECK(g.insert_edge(2, 3, "weight4"));
		CHECK(g.insert_edge(2, 4, "weight5"));
		CHECK(g.insert_edge(3, 4, "weight6"));

		auto start = g.end();
		auto end = g.end();

		auto check = g.erase_edge(start, end);
		CHECK(check == g.end());
	}

	SECTION("Range Erase Edge Iterator used when DST Iterator doesn't Exist") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3, 4};

		CHECK(g.insert_edge(1, 2, "weight1"));
		CHECK(g.insert_edge(1, 3, "weight2"));
		CHECK(g.insert_edge(1, 4, "weight3"));
		CHECK(g.insert_edge(2, 3, "weight4"));
		CHECK(g.insert_edge(2, 4, "weight5"));
		CHECK(g.insert_edge(3, 4, "weight6"));

		auto start = g.begin();
		start++;

		auto end = g.end();
		auto check = g.erase_edge(start, end);
		CHECK(check == g.end());
	}

	SECTION("Range Erase Edge Iterator used when SRC Iterator doesn't Exist") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3, 4};

		CHECK(g.insert_edge(1, 2, "weight1"));
		CHECK(g.insert_edge(1, 3, "weight2"));
		CHECK(g.insert_edge(1, 4, "weight3"));
		CHECK(g.insert_edge(2, 3, "weight4"));
		CHECK(g.insert_edge(2, 4, "weight5"));
		CHECK(g.insert_edge(3, 4, "weight6"));

		auto end = g.begin();
		end++;

		auto start = g.end();
		auto check = g.erase_edge(start, end);
		CHECK(check == g.end());
	}
}
