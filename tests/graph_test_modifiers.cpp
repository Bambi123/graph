#include "gdwg/graph.hpp"

#include <catch2/catch.hpp>

/*
Thomas French
2nd of August
COMP6771 Assignment 3
graph_test_modifiers.cpp

The following testing file, will detail the possible testing scenarios for the
modifiers methods for the class file graph.hpp.  The tests are designed based on
individual function calls and sections are designed based on specific scenarios
that could occur whilst using the library.  Edge cases are included in these
testing scenarios.
*/

TEST_CASE("Insert Node") {
	SECTION("Inserted Nodes Can be Added") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));

		CHECK(g.insert_node(4));
		CHECK(g.insert_node(5));
		CHECK(g.insert_node(6));

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));
		CHECK(g.is_node(4));
		CHECK(g.is_node(5));
		CHECK(g.is_node(6));
	}

	SECTION("Inserted Nodes Already Exist") {
		auto g = gdwg::graph<int, std::string>{1, 2, 3};

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));

		CHECK(!g.insert_node(1));
		CHECK(!g.insert_node(2));
		CHECK(!g.insert_node(3));
	}
}

TEST_CASE("Insert Edge") {
	SECTION("Inserted Edges Correctly") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 3, 15));
		CHECK(g.insert_edge(2, 3, 20));

		CHECK(g.edge_exists(1, 2, 10));
		CHECK(g.edge_exists(1, 3, 15));
		CHECK(g.edge_exists(2, 3, 20));
	}

	SECTION("Inserted Edges Already Exist") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 3, 15));
		CHECK(g.insert_edge(2, 3, 20));

		CHECK(!g.insert_edge(1, 2, 10));
		CHECK(!g.insert_edge(1, 3, 15));
		CHECK(!g.insert_edge(2, 3, 20));
	}

	SECTION("Inserted Edge SRC Doesn't Exist") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));

		CHECK_THROWS(g.insert_edge(4, 1, 10));
	}

	SECTION("Inserted Edge DST Doesn't Exist") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));

		CHECK_THROWS(g.insert_edge(1, 4, 10));
	}

	SECTION("Inserted Edge SRC and DST Doesn't Exist") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));

		CHECK_THROWS(g.insert_edge(4, 5, 10));
	}
}

TEST_CASE("Replace Node") {
	SECTION("Replace Node Used Correctly") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 3, 15));
		CHECK(g.insert_edge(2, 3, 20));

		CHECK(g.replace_node(1, 4));

		CHECK(g.is_node(4));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));

		CHECK(!g.edge_exists(1, 2, 10));
		CHECK(!g.edge_exists(1, 3, 15));

		CHECK(g.edge_exists(4, 2, 10));
		CHECK(g.edge_exists(4, 3, 15));
		CHECK(g.edge_exists(2, 3, 20));
	}

	SECTION("Replace Node but New Data already Exists") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 3, 15));
		CHECK(g.insert_edge(2, 3, 20));

		CHECK(!g.replace_node(1, 3));
	}

	SECTION("Replace Node but Old Data doesn't Exist") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 3, 15));
		CHECK(g.insert_edge(2, 3, 20));

		CHECK_THROWS(g.replace_node(4, 1));
	}
}

TEST_CASE("Merge Replace Node") {
	SECTION("Merge Replace Node Used Correctly") {
		auto g = gdwg::graph<std::string, int>{"A", "B", "C", "D"};
		CHECK(g.is_node("A"));
		CHECK(g.is_node("B"));
		CHECK(g.is_node("C"));
		CHECK(g.is_node("D"));

		CHECK(g.insert_edge("A", "B", 1));
		CHECK(g.insert_edge("A", "C", 2));
		CHECK(g.insert_edge("B", "C", 3));
		CHECK(g.insert_edge("C", "D", 4));
		CHECK(g.insert_edge("D", "B", 5));

		g.merge_replace_node("B", "A");

		CHECK(!g.is_node("B"));

		CHECK(g.edge_exists("A", "A", 1));
		CHECK(g.edge_exists("A", "C", 2));
		CHECK(g.edge_exists("A", "C", 3));
		CHECK(g.edge_exists("C", "D", 4));
		CHECK(g.edge_exists("D", "A", 5));

		CHECK(!g.is_node("B"));

		CHECK(!g.edge_exists("B", "C", 3));
		CHECK(!g.edge_exists("D", "B", 5));
	}

	SECTION("Merge Replace Node but old_data doesn't Exist") {
		auto g = gdwg::graph<std::string, int>{"A", "B", "C", "D"};
		CHECK(g.is_node("A"));
		CHECK(g.is_node("B"));
		CHECK(g.is_node("C"));
		CHECK(g.is_node("D"));

		CHECK_THROWS(g.merge_replace_node("E", "A"));
	}

	SECTION("Merge Replace Node but new_data doesn't Exist") {
		auto g = gdwg::graph<std::string, int>{"A", "B", "C", "D"};
		CHECK(g.is_node("A"));
		CHECK(g.is_node("B"));
		CHECK(g.is_node("C"));
		CHECK(g.is_node("D"));

		CHECK_THROWS(g.merge_replace_node("A", "E"));
	}

	SECTION("Merge Replace Node but old_data and new_data doesn't Exist") {
		auto g = gdwg::graph<std::string, int>{"A", "B", "C", "D"};
		CHECK(g.is_node("A"));
		CHECK(g.is_node("B"));
		CHECK(g.is_node("C"));
		CHECK(g.is_node("D"));

		CHECK_THROWS(g.merge_replace_node("F", "E"));
	}
}

TEST_CASE("Erase Node") {
	SECTION("Erase Node is used Correctly") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 3, 15));
		CHECK(g.insert_edge(2, 3, 20));
		CHECK(g.insert_edge(3, 1, 25));

		CHECK(g.erase_node(1));

		CHECK(!g.is_node(1));

		CHECK(!g.edge_exists(1, 2, 10));
		CHECK(!g.edge_exists(1, 3, 15));
		CHECK(g.edge_exists(2, 3, 20));
		CHECK(!g.edge_exists(3, 1, 25));
	}

	SECTION("Erase Node but the Node doesn't Exist") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.is_node(1));
		CHECK(g.is_node(2));
		CHECK(g.is_node(3));

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 3, 15));
		CHECK(g.insert_edge(2, 3, 20));
		CHECK(g.insert_edge(3, 1, 25));

		CHECK(!g.erase_node(4));
	}
}

TEST_CASE("Erase Edge") {
	SECTION("Erase Edge Used Correctly") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 3, 15));
		CHECK(g.insert_edge(2, 3, 20));
		CHECK(g.insert_edge(3, 1, 25));

		CHECK(g.erase_edge(1, 2, 10));

		CHECK(!g.edge_exists(1, 2, 10));
		CHECK(g.edge_exists(1, 3, 15));
		CHECK(g.edge_exists(2, 3, 20));
		CHECK(g.edge_exists(3, 1, 25));
	}

	SECTION("Erase Edge but SRC Node doesn't Exist") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 3, 15));
		CHECK(g.insert_edge(2, 3, 20));
		CHECK(g.insert_edge(3, 1, 25));

		CHECK_THROWS(g.erase_edge(4, 1, 10));
	}

	SECTION("Erase Edge but DST Node doesn't Exist") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 3, 15));
		CHECK(g.insert_edge(2, 3, 20));
		CHECK(g.insert_edge(3, 1, 25));

		CHECK_THROWS(g.erase_edge(1, 4, 10));
	}

	SECTION("Erase Edge but SRC and DST Node doesn't Exist") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 3, 15));
		CHECK(g.insert_edge(2, 3, 20));
		CHECK(g.insert_edge(3, 1, 25));

		CHECK_THROWS(g.erase_edge(5, 4, 10));
	}

	SECTION("Erase Edge but EDGE doesn't Exist") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 3, 15));
		CHECK(g.insert_edge(2, 3, 20));
		CHECK(g.insert_edge(3, 1, 25));

		CHECK(!g.erase_edge(1, 2, 15));
	}
}

TEST_CASE("Clear") {
	SECTION("Clear is ued Correctly") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, 10));
		CHECK(g.insert_edge(1, 3, 15));
		CHECK(g.insert_edge(2, 3, 20));
		CHECK(g.insert_edge(3, 1, 25));

		g.clear();
		CHECK(!g.is_node(1));
		CHECK(!g.is_node(2));
		CHECK(!g.is_node(3));
		CHECK(g.empty());

		CHECK(!g.edge_exists(1, 2, 10));
		CHECK(!g.edge_exists(1, 3, 15));
		CHECK(!g.edge_exists(2, 3, 20));
		CHECK(!g.edge_exists(3, 1, 25));
	}
}
