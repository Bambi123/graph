#include "gdwg/graph.hpp"

#include <catch2/catch.hpp>

/*
Thomas French
2nd of August
COMP6771 Assignment 3
graph_test_extractor.cpp

The following testing file, will detail the possible testing scenarios for the
extractor methods for the class file graph.hpp.  The tests are designed based on
individual function calls and sections are designed based on specific scenarios
that could occur whilst using the library.  Edge cases are included in these
testing scenarios.
*/

TEST_CASE("Extractor Overload") {
	SECTION("Extractor Overload is used correctly") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(g.insert_edge(1, 2, -1));
		CHECK(g.insert_edge(2, 3, 1));
		CHECK(g.insert_edge(2, 2, 7));

		auto oss = std::ostringstream{};
		oss << g;
		auto const expected_output = std::string_view(R"(1 (
  2 | -1
)
2 (
  2 | 7
  3 | 1
)
3 (
)
)");

		CHECK(oss.str() == expected_output);
	}

	SECTION("Extractor Overload but with a Graph with only Nodes") {
		auto g = gdwg::graph<int, int>{1, 2, 3};

		CHECK(!g.empty());

		auto oss = std::ostringstream{};
		oss << g;
		auto const expected_output = std::string_view(R"(1 (
)
2 (
)
3 (
)
)");
		CHECK(oss.str() == expected_output);
	}

	SECTION("Extractor Overload but with an Empty Graph") {
		auto g = gdwg::graph<int, int>{};

		CHECK(g.empty());

		auto oss = std::ostringstream{};
		oss << g;
		auto const expected_output = std::string_view(R"()");
		CHECK(oss.str() == expected_output);
	}
}
