/*
Thomas French
z5206283
COMP6771 - Assignment 3
graph.hpp

In mathematics, a graph is a set of nodes that are connected with an
accompanying set of edges, G(V, E).  The nodes are connected to one another with
a specific edge with an associated weight.

The following C++ library contains the class definition for a particular
weighted multi-directional graph.  This means that all edges have a weight, direction,
and multiple edges connected to the same nodes may exist with different weights.
The library provides a number of constructors, operators, accessors, modifiers,
and iterators required to engage the use of the graph as well as traverse the nodes.

Accompanying this file, is a set of C++ testing files used to ensure that the
operation of the library is correct.  These can be found under ass3/test/graph/.
*/

#include <iostream>
#include <set>
#include <sstream>
#include <utility>
#include <vector>

namespace gdwg {
	template<typename N, typename E>
	class graph {
	public:
		// EDGE TYPE DEFINITION
		struct path_type {
			std::weak_ptr<N> src;
			std::weak_ptr<N> dst;
			E weight;
		};
		// EDGE COMPARATOR
		struct path_comparator {
			bool operator()(const path_type& p1, const path_type& p2) const {
				// First comparison is from the src node
				if (*p1.src.lock() != *p2.src.lock()) {
					return *p1.src.lock() < *p2.src.lock();
				}
				// Second resort is the dst node
				if (*p1.dst.lock() != *p2.dst.lock()) {
					return *p1.dst.lock() < *p2.dst.lock();
				}
				// If the edge weights are the same, then this will never have been called
				// so can easily consider the edge weights different
				return p1.weight < p2.weight;
			}
		};
		// NODE COMPARATOR
		struct node_comparator {
			bool operator()(const std::shared_ptr<N>& p1, const std::shared_ptr<N>& p2) const {
				return *p1 < *p2;
			}
		};
		// VALUE TYPE DEFINITION
		struct value_type {
			N from;
			N to;
			E weight;
		};
		// TYPE DEFINITIONS FOR GRAPH ELEMENTS
		using node_type = std::set<std::shared_ptr<N>, node_comparator>;
		using edge_type = std::set<path_type, path_comparator>;

		// DEFAULT CONSTRUCTOR
		graph() {}

		// INITIALIZER LIST CONSTRUCTOR
		graph(std::initializer_list<N> il)
		: graph(il.begin(), il.end()) {}

		// INPUT ITERATOR CONSTRUCTOR
		template<typename InputIt>
		graph(InputIt first, InputIt last) {
			for (auto it = first; it != last; it++) {
				insert_node(*it);
			}
		}

		// GRAPH MOVE CONSTRUCTOR
		graph(graph&& other) noexcept {
			// Copy all nodes and edges
			// NODES
			for (auto it = other.nodes_.begin(); it != other.nodes_.end(); it++) {
				insert_node(**it); // double dereference
			}
			// EDGES
			for (auto it = other.edges_.begin(); it != other.edges_.end(); it++) {
				insert_edge(*it->src.lock(), *it->dst.lock(), it->weight);
			}
			// Now clear the previous graph completely
			other.clear();
		}

		// EQUALS MOVE CONSTRUCTOR
		auto operator=(graph&& other) noexcept -> graph& {
			// Clear previous contents of graph
			clear();
			// Copy all nodes and edges
			// NODES
			for (auto it = other.nodes_.begin(); it != other.nodes_.end(); it++) {
				insert_node(**it); // double dereference
			}
			// EDGES
			for (auto it = other.edges_.begin(); it != other.edges_.end(); it++) {
				insert_edge(*it->src.lock(), *it->dst.lock(), it->weight);
			}
			// Now clear the previous graph completely
			other.clear();
			return *this;
		}

		// GRAPH COPY CONSTRUCTOR
		graph(graph const& other) {
			// Copy all nodes and edges
			// NODES
			for (auto it = other.nodes_.begin(); it != other.nodes_.end(); it++) {
				insert_node(**it); // double dereference
			}
			// EDGES
			for (auto it = other.edges_.begin(); it != other.edges_.end(); it++) {
				insert_edge(*it->src.lock(), *it->dst.lock(), it->weight);
			}
		}

		// EQUALS COPY CONSTRUCTOR
		auto operator=(graph const& other) -> graph& {
			// Clear previous contents of graph
			clear();
			// Copy all nodes and edges
			// NODES
			for (auto it = other.nodes_.begin(); it != other.nodes_.end(); it++) {
				insert_node(**it); // double dereference
			}
			// EDGES
			for (auto it = other.edges_.begin(); it != other.edges_.end(); it++) {
				insert_edge(*it->src.lock(), *it->dst.lock(), it->weight);
			}
			return *this;
		}

		// INSERT NODE
		auto insert_node(N const& value) -> bool {
			// //Insert a NEW node if it doesn't alreayd exist
			if (is_node(value)) {
				return false;
			}
			nodes_.insert(std::make_shared<N>(value));
			return true;
		}

		// INSERT EDGE
		auto insert_edge(N const& src, N const& dst, E const& weight) -> bool {
			// Check that the src and dst node aren't already in the graph
			if (!is_node(src) or !is_node(dst)) {
				return throw_insert_edge();
			}
			// Check that the edge doesn't already exist
			if (edge_exists(src, dst, weight)) {
				return false;
			}
			// Then insert the edge straight into the private member
			edges_.insert(
			   {std::shared_ptr<N>(*get_node_ref(src)), std::shared_ptr<N>(*get_node_ref(dst)), weight});

			return true;
		}

		// REPLACE NODE
		auto replace_node(N const& old_data, N const& new_data) -> bool {
			// Check that the new data is in the graph
			if (!is_node(old_data)) {
				return throw_replace_node();
			}
			// Check that the new data isn't already in the graph
			if (is_node(new_data)) {
				return false;
			}
			// Double dereference to change the element's value
			**get_node_ref(old_data) = new_data;
			return true;
		}

		// MERGE REPLACE NODE
		auto merge_replace_node(N const& old_data, N const& new_data) -> void {
			// Check that both the old and new data nodes are in the graph
			if (!is_node(old_data) or !is_node(new_data)) {
				return throw_merge_replace_node();
			}

			// Whever there is an edge pointing from old_data, change to point from new_data
			for (auto it = edges_.begin(); it != edges_.end(); it++) {
				if (*it->src.lock() == old_data) {
					insert_edge(new_data, *it->dst.lock(), it->weight);
				}
			}
			// Whever there is an edge pointing to old_data, change to point to new_data
			for (auto it = edges_.begin(); it != edges_.end(); it++) {
				if (*it->dst.lock() == old_data) {
					insert_edge(*it->src.lock(), new_data, it->weight);
				}
			}

			// These need to be in separate loops, otherwise te iterators change

			// Remove the old data from the graph
			erase_node(old_data);
		}

		// ERASE NODE
		auto erase_node(N const& value) -> bool {
			// Check that the node is in the graph
			if (!is_node(value)) {
				return false;
			}

			// Remove all edges connected to or from the node
			auto it = edges_.begin();
			while (it != edges_.end()) {
				if (*it->src.lock() == value or *it->dst.lock() == value) {
					// store the previous instance of the address and increment
					// the iterator, then remove the old instance
					auto old = it;
					it++;
					erase_edge(*old->src.lock(), *old->dst.lock(), old->weight);
					continue;
				}
				it++;
			}
			// Then remove the node itself
			nodes_.erase(get_node_ref(value));
			return true;
		}

		// ERASE EDGE
		auto erase_edge(N const& src, N const& dst, E const& weight) -> bool {
			// Check that the nodes are both in the graph
			if (!is_node(src) or !is_node(dst)) {
				return throw_erase_edge();
			}
			// Find the edge and erase it
			for (auto it = edges_.begin(); it != edges_.end(); it++) {
				if (*it->src.lock() == src and *it->dst.lock() == dst and it->weight == weight) {
					edges_.erase(*it);
					return true;
				}
			}

			return false;
		}

		// CLEAR GRAPH
		auto clear() noexcept -> void {
			// Clear the nodes set
			nodes_.clear();
			// Clear the edges set
			edges_.clear();
		}

		// IS NODE: O(log(n))
		[[nodiscard]] auto is_node(N const& value) -> bool {
			// Check if we can find the iterator value for the node
			auto check = get_node_ref(value);
			// If not the end of the list, return true
			if (check != nodes_.end()) {
				return true;
			}
			// Otherwise, return false
			return false;
		}

		// IS EMPTY -> NONCONST
		[[nodiscard]] auto empty() -> bool {
			// Call the std::set empty method on nodes
			return nodes_.empty();
		}

		// CONNECTED NODES
		[[nodiscard]] auto is_connected(N const& src, N const& dst) -> bool {
			// Check that both src and dst are in the graph
			if (!is_node(src) or !is_node(dst)) {
				return throw_is_connected();
			}

			// Find if there is a connection between src and dst
			for (auto it = edges_.begin(); it != edges_.end(); it++) {
				if (*it->src.lock() == src and *it->dst.lock() == dst) {
					return true;
				}
			}
			return false;
		}

		// VECTOR OF NODES
		[[nodiscard]] auto nodes() -> std::vector<N> {
			auto v = std::vector<N>{};
			for (auto it = nodes_.begin(); it != nodes_.end(); it++) {
				v.insert(v.end(), **it);
			}
			return v;
		}

		// VECTOR OF WEIGHTS
		[[nodiscard]] auto weights(N const& src, N const& dst) -> std::vector<E> {
			// Check that the src and dst nodes are in the graph
			if (!is_node(src) or !is_node(dst)) {
				return throw_weights();
			}

			// Add all the edge weights to the edges vector set
			auto v = std::vector<E>{};
			for (auto it = edges_.begin(); it != edges_.end(); it++) {
				if (*it->src.lock() == src and *it->dst.lock() == dst) {
					v.insert(v.end(), it->weight);
				}
			}
			return v;
		}

		// CONNECTIONS
		[[nodiscard]] auto connections(N const& src) -> std::vector<N> {
			// Check that the src node is in the graph
			if (!is_node(src)) {
				return throw_connections();
			}

			// Add all the connected nodes to the nodes vector
			auto v = std::vector<N>{};
			for (auto it = edges_.begin(); it != edges_.end(); it++) {
				if (*it->src.lock() == src) {
					if (std::find(v.begin(), v.end(), *it->dst.lock()) != v.end()) {
						continue;
					}
					v.insert(v.end(), *it->dst.lock());
				}
			}
			return v;
		}

		// COMPARISON EQUALS
		[[nodiscard]] auto operator==(graph const& other) const -> bool {
			// Check and compare the sizes of each set in each graph
			if (nodes_.size() != other.nodes_.size()) {
				return false;
			}
			if (edges_.size() != other.edges_.size()) {
				return false;
			}

			// Check all the node values
			auto it_n1 = nodes_.begin();
			auto it_n2 = other.nodes_.begin();
			while (it_n1 != nodes_.end() and it_n2 != other.nodes_.end()) {
				if (**it_n1 != **it_n2) {
					return false;
				}
				it_n1++;
				it_n2++;
			}
			// Check all the edge values
			auto it_e1 = edges_.begin();
			auto it_e2 = other.edges_.begin();
			while (it_e1 != edges_.end() and it_e2 != other.edges_.end()) {
				if (*it_e1->src.lock() != *it_e2->src.lock()) {
					return false;
				}
				if (*it_e1->dst.lock() != *it_e2->dst.lock()) {
					return false;
				}
				if (it_e1->weight != it_e2->weight) {
					return false;
				}
				it_e1++;
				it_e2++;
			}
			// If we pass all these criteria, then the graphs are equal
			return true;
		}

		// OUTSTREAM EXTRACTOR
		friend auto operator<<(std::ostream& os, graph const& g) -> std::ostream& {
			// Cycle through nodes and add all edges accordingly
			for (auto it = g.nodes_.begin(); it != g.nodes_.end(); it++) {
				os << **it << " (\n";
				for (auto ed = g.edges_.begin(); ed != g.edges_.end(); ed++) {
					if (*ed->src.lock() == **it) {
						os << "  " << *ed->dst.lock() << " | " << ed->weight << "\n";
					}
				}
				os << ")\n";
			}
			return os;
		}

		// template<typename N, typename E>
		class iterator {
		public:
			using value_type = graph<N, E>::value_type;
			using reference = value_type;
			using pointer = void;
			using difference_type = std::ptrdiff_t;
			using iterator_category = std::bidirectional_iterator_tag;

			using iterator_type = typename edge_type::const_iterator;

			// ITERATOR DEFAULT CONSTRUCTOR
			iterator() = default;

			// ITERATOR GENERAL CONSTRUCTOR
			iterator(iterator_type iter, iterator_type end)
			: iter_(iter)
			, end_(end) {}

			// Iterator source
			auto operator*() -> reference {
				return {*iter_->src.lock(), *iter_->dst.lock(), iter_->weight};
			}

			// INCREMENT ITERATOR
			auto operator++() -> iterator& {
				if (iter_ == end_) {
					return *this;
				}
				iter_++;
				return *this;
			}

			// INTEGER INCREMENT ITERATOR
			auto operator++(int) -> iterator {
				// if we're at the end, it should be undefined, but just in case:
				if (iter_ == end_) {
					return *this;
				}
				// if at the end, returns the end again, should never be called anyway
				auto temp = *this;
				iter_++;
				return temp;
			}

			// DECREMENT ITERATOR
			auto operator--() -> iterator& {
				// don't consider begin() behaviour, consider it undefined
				iter_--;
				return *this;
			}

			// INTEGER DECREMENT ITERATOR
			auto operator--(int) -> iterator {
				// don't consider begin() behaviour, consider it undefined
				auto temp = *this;
				iter_--;
				return temp;
			}

			// Iterator comparison
			auto operator==(iterator const& other) const -> bool {
				// Can't compare elements of null iterators, so do manually
				if (iter_ == end_ and other.iter_ == other.end_) {
					return true;
				}
				if (iter_ == end_ or other.iter_ == other.end_) {
					return false;
				}

				auto v1 = iter_;
				auto v2 = other.iter_;
				if (*v1->src.lock() != *v2->src.lock()) {
					return false;
				}
				if (*v1->dst.lock() != *v2->dst.lock()) {
					return false;
				}
				if (v1->weight != v2->weight) {
					return false;
				}
				return true;
			}

		private:
			// explicit iterator(unspecified);
			iterator_type iter_;
			iterator_type end_; // saves the ending iterator
		};

		// ITERATORS

		// ITERATOR BEGIN
		[[nodiscard]] auto begin() const -> iterator {
			return iterator(edges_.begin(), edges_.end());
		}

		// ITERATOR END
		[[nodiscard]] auto end() const -> iterator {
			return iterator(edges_.end(), edges_.end());
		}

		// FIND ITERATOR
		[[nodiscard]] auto find(N const& src, N const& dst, E const& weight) -> iterator {
			// Check edges set to find the corresponding src, dst and weight
			for (auto it = edges_.begin(); it != edges_.end(); it++) {
				if (*it->src.lock() == src and *it->dst.lock() == dst and it->weight == weight) {
					return iterator(it, edges_.end());
				}
			}
			// If src, dst, weight doesn't exist, return end iterator
			return iterator(edges_.end(), edges_.end());
		}

		// ERASE EDGE ITERATOR
		auto erase_edge(iterator i) -> iterator {
			if (i == end()) {
				return iterator(edges_.end(), edges_.end());
			}

			// Assign the next value and increment the i value
			// Then remove the next value
			auto next = i++;
			auto v = *next;
			// Erase the edge with these values
			erase_edge(v.from, v.to, v.weight);
			return i;
		}

		// ERASE RANGE EDGES ITERATOR
		auto erase_edge(iterator i, iterator s) -> iterator {
			// Check that i and s aren't the end iterators
			// If i and s are inaccessible memory,
			// consider undefined behaviour
			if (i == end() or s == end()) {
				return end();
			}
			// Loop through until either it
			auto it = i;
			while (!(it == s) and !(it == end())) {
				it = erase_edge(it);
			}
			if (it == end()) {
				return end();
			}

			return s;
		}

		// HELPERS

		// GETS THE POINTER TO THE NODE IN NODES_ : O(log(n))
		auto get_node_ref(N const& n) -> typename node_type::iterator {
			return std::find_if(nodes_.begin(), nodes_.end(), [n](auto p) { return *p == n; });
		}

		// CHECKS IF THERE IS AN EXISTING EDGE
		auto edge_exists(N const& s, N const& d, E const& w) -> bool {
			for (auto it = edges_.begin(); it != edges_.end(); it++) {
				if (*it->src.lock() == s and *it->dst.lock() == d and it->weight == w) {
					return true;
				}
			}
			return false;
		}

		// THROWS EXCEPTION MESSAGE FOR INSERT EDGE
		auto throw_insert_edge() -> bool {
			throw std::runtime_error("Cannot call gdwg::graph<N, E>::insert_edge when either src or "
			                         "dst node does not exist");
			return false;
		}

		// THROWS EXCEPTION MESSAGE FOR REPLACE NODE
		auto throw_replace_node() -> bool {
			throw std::runtime_error("Cannot call gdwg::graph<N, E>::replace_node on a node that "
			                         "doesn't exist");
			return false;
		}

		// THROWS EXCEPTION MESSAGE FOR ERASE EDGE
		auto throw_erase_edge() -> bool {
			throw std::runtime_error("Cannot call gdwg::graph<N, E>::erase_edge on src or dst if they "
			                         "don't exist in the graph");
			return false;
		}

		// THROWS EXCEPTION MESSAGE FOR MERGE REPLACE NODE
		auto throw_merge_replace_node() -> void {
			throw std::runtime_error("Cannot call gdwg::graph<N, E>::merge_replace_node on old or new "
			                         "data if they don't exist in the graph");
			return;
		}

		// THROW EXCEPTION MESSAGE FOR IS CONNECTED
		auto throw_is_connected() -> bool {
			throw std::runtime_error("Cannot call gdwg::graph<N, E>::is_connected if src or dst node "
			                         "don't exist in the graph");
			return false;
		}

		// THROW EXCEPTION MESSAGE FOR WEIGHTS
		auto throw_weights() -> std::vector<E> {
			throw std::runtime_error("Cannot call gdwg::graph<N, E>::weights if src or dst node don't "
			                         "exist in the graph");
			return {};
		}

		// THROW EXCEPTION MESSAGE FOR CONNECTIONS
		auto throw_connections() -> std::vector<N> {
			throw std::runtime_error("Cannot call gdwg::graph<N, E>::connections if src doesn't exist "
			                         "in the graph");
			return {};
		}

	private:
		node_type nodes_;
		edge_type edges_;
	};

} // namespace gdwg

